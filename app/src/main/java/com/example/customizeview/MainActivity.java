package com.example.customizeview;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "MainActivity";

    private Button mButton;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final View view = findViewById(R.id.circleView);

        mButton = findViewById(R.id.start_frame_animation);
        mButton.setOnClickListener(this);
        mButton.setBackgroundResource(R.drawable.frame_animation);

        Button startLayoutAnimationActivity = findViewById(R.id.startLayoutAnimationActivity);
        startLayoutAnimationActivity.setOnClickListener(this);

        Button startAnimateLayoutChanges = findViewById(R.id.startAnimateLayoutChangesActivity);
        startAnimateLayoutChanges.setOnClickListener(this);

        Button testBitmapSize = findViewById(R.id.startTestBitmapSize);
        testBitmapSize.setOnClickListener(this);

        // 手工测量
        // 1.当View的layoutParams为matchParent时，因为无法知道parentSize所以这种方法没办法进行测量
        // 2.当View的layoutParams为具体数值的时候，使用MeasureSpec构造相应的规则
        int widthWithMS = View.MeasureSpec.makeMeasureSpec(100, View.MeasureSpec.EXACTLY);
        int heightWithMS = View.MeasureSpec.makeMeasureSpec(100, View.MeasureSpec.EXACTLY);
//        view.measure(widthWithMS, heightWithMS);
        Log.i(TAG, "dp: widthWithMS" + view.getMeasuredWidth() + " heightWithMS " + view.getMeasuredHeight());
        // 3.当view的layoutParams为wrap_content的时候，使用MeasureSpec支持的最大的size去构造测量,因为view没办法超过1<<30-1
        int maxSizeMSWidth = View.MeasureSpec.makeMeasureSpec(1 << 30 - 1, View.MeasureSpec.AT_MOST);
        int maxSizeMSHeight = View.MeasureSpec.makeMeasureSpec(1 << 30 - 1, View.MeasureSpec.AT_MOST);
        view.measure(maxSizeMSWidth, maxSizeMSHeight);
        Log.i(TAG, "wrap_content: widthWithMS" + view.getMeasuredWidth() + " heightWithMS " + view.getMeasuredHeight());

        // 通过View的post方法获取view的宽度和高度
        view.post(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "post: view.getWidth()" + view.getWidth());
                Log.i(TAG, "post: view.getHeight()" + view.getHeight());
            }
        });


        // 注册ViewTreeObserver进行监听
        ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    // 伴随着view树状态的改变，这个接口会被回调多次，这里所以要取消注册
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    Log.i(TAG, "onWindowFocusChanged: view.getWidth()" + view.getWidth());
                    Log.i(TAG, "onWindowFocusChanged: view.getHeight()" + view.getHeight());
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart: ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume: ");
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    // 根据key读取value 读取properties文件
    public static String readValue(String filePath, String key) {
        Properties props = new Properties();
        try {
            InputStream in = new BufferedInputStream(new FileInputStream(filePath));
            props.load(in);
            String value = props.getProperty(key, "");
            System.out.println(key + value);
            return value;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // 写properties文件 往properties文件中写入数据
    public static void writeProperties(String filePath, String parameterName, String parameterValue) {
        Properties prop = new Properties();
        try {
            InputStream fis = new FileInputStream(filePath);
            // 从输入流中读取属性列表（键和元素对）
            prop.load(fis);
            // 调用 Hashtable 的方法 put。使用 getProperty 方法提供并行性。
            // 强制要求为属性的键和值使用字符串。返回值是 Hashtable 调用 put 的结果。
            OutputStream fos = new FileOutputStream(filePath);
            prop.setProperty(parameterName, parameterValue);
            // 以适合使用 load 方法加载到 Properties 表中的格式，
            // 将此 Properties 表中的属性列表（键和元素对）写入输出流
            prop.store(fos, "Update '" + parameterName + "' value");
        } catch (IOException e) {
            System.err.println("Visit " + filePath + " for updating " + parameterName + " value error");
        }
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            View view = findViewById(R.id.circleView);
            Log.i(TAG, "onWindowFocusChanged: view.getWidth()" + view.getWidth());
            Log.i(TAG, "onWindowFocusChanged: view.getHeight()" + view.getHeight());
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.start_frame_animation) {
            AnimationDrawable animationDrawable = (AnimationDrawable) mButton.getBackground();
            if (animationDrawable.isRunning()) {
                animationDrawable.stop();
            } else {
                animationDrawable.start();
            }
        } else if (v.getId() == R.id.startLayoutAnimationActivity) {
            Intent intent = new Intent(this, LayoutAnimationActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_anim, R.anim.activity_stay);
        } else if (v.getId() == R.id.startAnimateLayoutChangesActivity) {
            Intent intent = new Intent(this, AnimateLayoutChanges.class);
            startActivity(intent);
        } else if (v.getId() == R.id.startTestBitmapSize) {
            Intent intent = new Intent(this, TestBitmapSize.class);
            startActivity(intent);
        }
    }
}
