package com.example.customizeview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewStub;

public class LayoutPerformanceActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "LayoutPerformanceActivi";

    private ViewStub mViewStub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout_performance);
        findViewById(R.id.showViewStub).setOnClickListener(this);
        findViewById(R.id.hideViewStub).setOnClickListener(this);
        mViewStub = findViewById(R.id.stub_import);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.showViewStub) {
            mViewStub.setVisibility(View.VISIBLE);
        } else if (v.getId() == R.id.hideViewStub) {
            mViewStub.setVisibility(View.INVISIBLE);
        } else {
            Log.i(TAG, "onClick: else");
        }
    }
}
